var express = require("express");
var app = express();

app.set("view engine", "pug");
app.set("views", "./views");

app.get("/", function(req, res) {
  res.render("content-before");
});

app.get("/auth/google", function(req, res) {
  res.render("content-after", {
    firstName: req.query.firstName,
    lastName: req.query.lastName
  });
});

app.listen(3000);
app.use(function(req, res, next) {
  res.status(404).send("Sorry, but there is no such a user in our newsteller!");
});
